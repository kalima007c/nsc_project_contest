import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';

class CommandDict extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Kanit'),
      home: Scaffold(
        appBar: AppBar(
          title: Text('สมุดคำสั่ง'),
        ),
        body: CommandBlock(),
      ),
    );
  }
}

class CommandBlock extends StatefulWidget {
  @override
  _CommandBlockState createState() => _CommandBlockState();
}

class _CommandBlockState extends State<CommandBlock> {
  @override
  void initState() {
    Speak('นี่คือรายการคำสั่งที่ท่านสามารถใช้งานฉันได้');
    
   
  }
  
  final FlutterTts flutterTts = FlutterTts();

  void Speak(text) async {
    await flutterTts.setLanguage("th-TH");
    await flutterTts.speak(text);
  }

  final double textSize = 18.00;

  final double spaces = 20.00;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: 20,
        horizontal: 20,
      ),
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          Text(
            'นี่คือรายการคำสั่งที่ท่านสามารถใช้งานฉันได้',
            style: TextStyle(fontSize: 18),
          ),
          SizedBox(
            height: 30,
          ),
          Text(
            '"ถามชื่อโรงพยาบาลที่ใกล้ที่สุด"',
            style: TextStyle(fontSize: textSize),
          ),
          SizedBox(
            height: spaces,
          ),
          Text(
            '"ถามแนวทางการดูแลตนเอง"',
            style: TextStyle(fontSize: textSize),
          ),
          
          SizedBox(
            height: spaces,
          ),
          Text(
            '"ถามปัญหาทั่ว ๆ ไป"',
            style: TextStyle(fontSize: textSize),
          ),
          SizedBox(
            height: spaces,
          ),
          Text(
            '"หมายเลขฉุกเฉิน"',
            style: TextStyle(fontSize: textSize),
          ),
          
          SizedBox(
            height: spaces,
          ),
          Text(
            '"เบาหวานดูแลตัวเองอย่างไร"',
            style: TextStyle(fontSize: textSize),
          ),
          
          SizedBox(
            height: spaces,
          ),
          Text(
            '"เบาหวานห้ามทานอะไร"',
            style: TextStyle(fontSize: textSize),
          ),
          
          SizedBox(
            height: spaces,
          ),
        ],
      ),
    );
  }
}
