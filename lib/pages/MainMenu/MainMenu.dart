import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:nsc_contest_robot/pages/Assistant/Assistant.dart';
import 'package:nsc_contest_robot/pages/ChatBot/Chatbot.dart';
import 'package:nsc_contest_robot/pages/Controller/ControlMenu.dart';
import 'package:nsc_contest_robot/pages/Dictionary/Dictionary.dart';

class MainMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('เมนูหลัก'),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 50,
            ),
            ImageComponent(),
            MainMenuComponents(),
          ],
        ),
      ),
    );
  }
}

class MainMenuComponents extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const double ButtonHeight = 25;
    const double ButtonVertSpace = 25;
    const double ButtonFontSize = 18;
    return Container(
      margin: const EdgeInsets.fromLTRB(30, 50, 35, 30),
      child: Column(
        children: <Widget>[
          RaisedButton(
            padding:
                const EdgeInsets.fromLTRB(10, ButtonHeight, 10, ButtonHeight),
            color: Colors.blueAccent,
            child: Row(
              children: <Widget>[
                Icon(Icons.book, color: Colors.white),
                SizedBox(width: 20),
                Text(
                  "สมุดคำสั่ง",
                  style:
                      TextStyle(color: Colors.white, fontSize: ButtonFontSize),
                )
              ],
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MaterialApp(
                            home: CommandDict(),
                          )));
            },
          ),
          SizedBox(
            height: ButtonVertSpace,
          ),
          RaisedButton(
            padding:
                const EdgeInsets.fromLTRB(10, ButtonHeight, 10, ButtonHeight),
            color: Colors.blueAccent,
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.chat,
                  color: Colors.white,
                ),
                SizedBox(width: 20),
                Text(
                  "ทดสอบ Intent ใหม่",
                  style:
                      TextStyle(color: Colors.white, fontSize: ButtonFontSize),
                )
              ],
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MaterialApp(
                            home: ChatBot(),
                          )));
            },
          ),
          SizedBox(
            height: ButtonVertSpace,
          ),
          RaisedButton(
            padding:
                const EdgeInsets.fromLTRB(10, ButtonHeight, 10, ButtonHeight),
            color: Colors.blueAccent,
            child: Row(
              children: <Widget>[
                Icon(Icons.mic, color: Colors.white),
                SizedBox(width: 20),
                Text(
                  "โหมดผู้ช่วยส่วนบุคคล",
                  style:
                      TextStyle(color: Colors.white, fontSize: ButtonFontSize),
                )
              ],
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MaterialApp(
                            home: Assistant(),
                          )));
            },
          ),
          SizedBox(
            height: ButtonVertSpace,
          ),
          RaisedButton(
            padding:
                const EdgeInsets.fromLTRB(10, ButtonHeight, 10, ButtonHeight),
            color: Colors.blueAccent,
            child: Row(
              children: <Widget>[
                Icon(Icons.play_arrow, color: Colors.white),
                SizedBox(width: 20),
                Text(
                  "โหมดควบคุม",
                  style:
                      TextStyle(color: Colors.white, fontSize: ButtonFontSize),
                )
              ],
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MaterialApp(
                            home: ControlMenu(),
                          )));
            },
          ),
        ],
      ),
    );
  }
}

class ImageComponent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Image(
      image: AssetImage('images/robot-vector.png'),
      width: 200,
      height: 200,
    );
  }
}
