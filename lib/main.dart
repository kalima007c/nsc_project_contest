import 'package:flutter/material.dart';
import 'package:nsc_contest_robot/pages/MainMenu/MainMenu.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MainMenu(),
      theme: new ThemeData(fontFamily: 'Kanit'),
    );
  }
}
